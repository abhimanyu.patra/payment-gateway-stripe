package com.stripe;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;


@SpringBootApplication
public class PaymentGatewayStripeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentGatewayStripeApplication.class, args);
	}

}
