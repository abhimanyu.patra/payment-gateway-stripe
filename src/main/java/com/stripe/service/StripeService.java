package com.stripe.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.stripe.Stripe;
import com.stripe.dto.ChargeRequest;
import com.stripe.entity.Order;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import com.stripe.param.PaymentIntentCreateParams.ConfirmationMethod;
import com.stripe.repository.OrderRepository;

@Service
public class StripeService {

	@Value("${STRIPE_SECRET_KEY}")
	String secretKey;

	@Autowired
	private OrderRepository orderRepository;

	@PostConstruct
	public void init() {
		Stripe.apiKey = secretKey;
	}

	public PaymentIntent payment(ChargeRequest chargeRequest) throws StripeException {

		PaymentIntentCreateParams params = PaymentIntentCreateParams.builder().setAmount(new Long(chargeRequest.getAmount()))
				.setCurrency("inr").setDescription(chargeRequest.getDescription()).setPaymentMethod("pm_card_in")
				.setConfirm(true)
				.setConfirmationMethod(ConfirmationMethod.AUTOMATIC)
				// .setReturnUrl("https://localhost:8443/confirmed")
				.build();

		PaymentIntent paymentIntent = PaymentIntent.create(params);

		return paymentIntent;
	}
	
	public Order createOrder(String id) throws StripeException {
		PaymentIntent paymentIntent = PaymentIntent.retrieve(id);
		
		Order order = new Order();
		order.setAmount(paymentIntent.getAmount());
		order.setStatus(paymentIntent.getStatus());
		order.setTransactionId(paymentIntent.getCharges().getData().get(0).getBalanceTransaction());

		// save order
		order = orderRepository.save(order);

		return order;
	}
	
}
