package com.stripe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.stripe.dto.ChargeRequest;
import com.stripe.dto.ChargeRequest.Currency;
import com.stripe.dto.Result;
import com.stripe.entity.Order;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.service.StripeService;

@RestController
@CrossOrigin(origins = {"https://localhost:3000"})
public class ChargeController {
	
	@Value("${STRIPE_PUBLIC_KEY}")
	private String stripePublicKey;

	@Autowired
	StripeService paymentsService;
	
	@PostMapping("/get-payment-intent")
	public ChargeRequest getPaymentIntent(@RequestBody ChargeRequest chargeRequest) throws StripeException {

		chargeRequest.setDescription("Farmer Joe Payment Description");
		chargeRequest.setCurrency(Currency.EUR.toString());
		PaymentIntent paymentIntent = paymentsService.payment(chargeRequest);
		chargeRequest.setOrderId(paymentIntent.getId());
		chargeRequest.setStripePublicKey(stripePublicKey);
		chargeRequest.setClientSecret(paymentIntent.getClientSecret());
		
		return chargeRequest;
		
	}

	@PostMapping("/get-order-details")
	public Result createOrder(@RequestBody ChargeRequest chargeRequest) throws StripeException {
		
		Order order = paymentsService.createOrder(chargeRequest.getOrderId());
		Result result = new Result();
		
		result.setOrderId(order.getOrderId().toString());
		result.setStatus(order.getStatus());
		result.setAmount(order.getAmount().toString());
		result.setTxnId(order.getTransactionId());
		
		return result;
	}

	@ExceptionHandler(StripeException.class)
	public String handleError(Model model, StripeException ex) {
		return ex.getMessage();
	}
	
}
